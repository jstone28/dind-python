# Docker in Docker - Python

## image tags:

* 2.7.15
* 3.6.7
* 3.7.3
* 3.8.0
* latest (always the newest version of python)

## Adding Versions

Peruse the [official python builds](https://www.python.org/ftp/python/). Add the desired version to the python_versions.csv.
The first entry should be the full version to include, the second entry should be the related gpg key for the release. More information on gpg keys can be found [in the official python docker image's update script](https://github.com/docker-library/python/blob/master/update.sh). The third entry is the version of pip to include.

Run the image updater script:

```
bash build.sh
```

Commit your changes and make a pull request.
